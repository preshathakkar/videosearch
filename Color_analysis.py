# import the necessary packages
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import argparse, cv2, os

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dir", required = True, help = "name of the dir")
ap.add_argument("-c", "--clusters", required = True, type = int,
	help = "# of clusters")
args = vars(ap.parse_args())


files = []
for file in os.listdir("/home/akshay/Desktop/576_Pro/CSCI576_Project_Database/"+args["dir"]):
    if file.endswith(".rgb"):
        files.append(file)

files.sort()
count = 0
for file in files:
    a = np.fromfile('/home/akshay/Desktop/576_Pro/CSCI576_Project_Database/'+args['dir']+'/'+file,dtype=np.uint8)
    ind = 0
    height = 288
    width = 352
    r=[]
    g=[]
    b=[]
    for y in range(height):
        for x in range(width):       
            r.append(a[ind])
            g.append(a[ind+height*width])
            b.append(a[ind+height*width*2])
            ind = ind+1
    big = []
    q = []
    for i in range(1,len(r)):
        if i%352==0:
            big.append(q)
            q=[]
        else:
            p = [b[i],g[i],r[i]]
            q.append(p)
            
    image = np.array(big)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # reshape the image to be a list of pixels
    image = image.reshape((image.shape[0] * image.shape[1], 3))

    # cluster the pixel intensities
    clt = KMeans(n_clusters = args['clusters'])
    clt.fit(image)
    count = count+1
    rgbval = clt.cluster_centers_[0]
    print rgbval
    
