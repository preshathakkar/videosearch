# import the necessary packages
from matplotlib import pyplot as plt
import numpy as np
from scipy import misc
import argparse
import cv2,copy
import SimpleCV
 
# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help = "Path to the image")
args = vars(ap.parse_args())

a = np.fromfile('flowers001.rgb',dtype=np.uint8)
ba = copy.deepcopy(a)
ba.shape = (len(ba)/3,3)
print ba[0]
ind = 0
height = 288
width = 352
r=[]
g=[]
b=[]
for y in range(height):
    for x in range(width):       
        r.append(a[ind])
        g.append(a[ind+height*width])
        b.append(a[ind+height*width*2])
        ind = ind+1

q = []
for j in range(288):
    p = []
    for i in range(352):
        p.append([b[i*j],g[i*j],r[i*j]])
    q.append(p)
            
image = np.array(q)
##plt.hist(rn.ravel(),256,[0,256]); 
##gn = np.asarray(g)
##bn = np.asarray(b)

print image.shape
print image

##color = ('b','g','r')
##for i,col in enumerate(color):
##    histr = cv2.calcHist([rn],[i],None,[256],[0,256])
##    plt.plot(histr,color = col)
##    plt.xlim([0,256])
##plt.show()
##rn.shape = (288,352)
##print type(rn)

##print rn.shape
cv2.imshow("image", image)

### convert the image to grayscale and create a histogram
#gray = cv2.cvtColor(rn, cv2.COLOR_BGR2GRAY)
#print gray
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("gray", gray)
hist = cv2.calcHist([gray], [0], None, [256], [0, 256])
plt.figure()
plt.title("Grayscale Histogram")
plt.xlabel("Bins")
plt.ylabel("# of Pixels")
plt.plot(hist)
plt.xlim([0, 256])
plt.show()
#hist = cv2.calcHist([gray], [1], None, [256], [0, 256])
plt.figure()
plt.title("Grayscale Histogram")
plt.xlabel("Bins")
plt.ylabel("# of Pixels")
#plt.plot(hist)
#plt.xlim([0, 256])
#plt.show()

# grab the image channels, initialize the tuple of colors,
# the figure and the flattened feature vector
chans = cv2.split(image)
colors = ("b", "g", "r")
plt.figure()
plt.title("'Flattened' Color Histogram")
plt.xlabel("Bins")
plt.ylabel("# of Pixels")
features = []
 
# loop over the image channels
for (chan, color) in zip(chans, colors):
	# create a histogram for the current channel and
	# concatenate the resulting histograms for each
	# channel
	hist = cv2.calcHist([chan], [0], None, [256], [0, 256])
	features.extend(hist)
 
	# plot the histogram
	plt.plot(hist, color = color)
	plt.xlim([0, 256])
 
# here we are simply showing the dimensionality of the
# flattened color histogram 256 bins for each channel
# x 3 channels = 768 total values -- in practice, we would
# normally not use 256 bins for each channel, a choice
# between 32-96 bins are normally used, but this tends
# to be application dependent
print "flattened feature vector size: %d" % (np.array(features).flatten().shape)
plt.show()
