__author__ = 'arjunpola'

import Config
import scipy as sp
import numpy as np
import redis
from scipy.io import wavfile

r = redis.StrictRedis(host='localhost', port=6379, db=0)
n = 17
fs, snd = wavfile.read(Config.getAudioPath(n))
snd = snd / (2.**15)
mFreq = []
result = []
max = 0
SAMPLE_SIZE = 20

if len(snd) >= (SAMPLE_SIZE * fs):
    s1 = snd[0:(SAMPLE_SIZE * fs), 0]
else:
    s1 = snd[0:len(snd), 0]
    numPad = (SAMPLE_SIZE * fs) - len(snd)
    # print numPad
    s1 = np.lib.pad(s1, (numPad, 0), mode='constant')

# print fs
# print len(s1)
# print snd.shape[0]
# print snd.dtype


def get_max_sub_sequence(data, seq):
    subSeq = []
    ind = 0

    for i in range(0, len(data) - len(seq), 1):
        diff = map(lambda x, y: abs(x-y), data[i:(i+len(seq))], seq)
        avg = reduce(lambda x, y: x+y, diff)/len(diff)
        subSeq.append(avg)

    subSeq = np.array(subSeq)
    subSeq = np.round(subSeq, decimals=4)
    return subSeq.min()

for idx in range(0, 2 * SAMPLE_SIZE, 1):
    start = idx * fs/2

    if idx <= 2 * SAMPLE_SIZE - 1:
        end = (idx+1) * fs/2
    else:
        end = (2 * SAMPLE_SIZE - 1) * fs/2

    chunk = s1[start:end]

    # N = len(chunk)
    # T = 1.0 / fs
    # x = chunk
    # y = np.sin(50.0 * 2.0*np.pi*x) + 0.5*np.sin(80.0 * 2.0*np.pi*x)
    yf = sp.fft(chunk)
    # xf = np.linspace(0.0, 1.0/(2.0*T), N/2)
    # YY = 2.0/N * np.abs(yf[0:N/2])
    YY = abs(yf)
    mFreq.append(YY.max())
    if YY.max() > max:
        max = YY.max()
    # print YY.max()
    # plt.plot(xf, YY)
    # plt.savefig("chunk"+str(idx)+".png")
    # plt.clf()


    # # -----------Working------------
    # timeArray = np.arange(0, float(len(chunkFFT)), 1)
    # timeArray = timeArray / fs
    # timeArray = timeArray * 1000  #scale to milliseconds
    #
    # n = len(chunkFFT)
    #
    # nUniquePts = np.ceil((n+1)/2.0)
    #
    # plt.plot(timeArray, chunkFFT, color='k')
    # plt.ylabel('Amplitude')
    # plt.xlabel('Time (ms)')
    #
    # plt.title('Time')
    # plt.savefig("chunk"+str(idx)+".png")
    # plt.show()

mFreq = np.array(mFreq)
if n <= 7:
    r.set(n, max)
    mFreq = mFreq / max
    mFreq = np.round(mFreq, decimals=2)
    # print mFreq
    r.set("Data"+str(n), ' '.join(np.array(mFreq, dtype=str).tolist()))
else:
    MaxMatch = [-1]
    for i in range(1, 8, 1):
        NormFreq = mFreq / float(r.get(i))
        NormFreq = np.round(NormFreq, decimals=2)
        NormFreq = np.array(filter(lambda x: x > 0.01, NormFreq[:]))
        # print" i = "+str(i)+" \nData: "
        # print NormFreq
        # print "\n"
        strData = r.get('Data'+str(i)).split(' ')
        data = []

        for d in strData:
            data.append(float(d))

        data = np.array(data)
        # print data
        # v = ' '.join(np.array(NormFreq, dtype=str).tolist())
        # print v
        #print i, get_max_sub_sequence(data, NormFreq)
        result.append(get_max_sub_sequence(data, NormFreq))
        # if len(m) == 0:
        #     MaxMatch.append(0)
        # else:
        #     MaxMatch.append(np.array(m).max())

result = np.array(result)
max = result.max()
result = 1 - (result/max)
for i in range(0, 7, 1):
    print i, result[i]