import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import argparse, cv2, os

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dir", required = True, help = "name of the dir")
args = vars(ap.parse_args())

kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
fgbg = cv2.BackgroundSubtractorMOG2()

files = []
for file in os.listdir("/home/akshay/Desktop/576_Pro/CSCI576_Project_Database/"+args["dir"]):
    if file.endswith(".rgb"):
        files.append(file)

files.sort()
count = 0
chunk = 0
sum=0
sum1 = 0
sum2 = 0
chunk_size = 10
for file in files:
    a = np.fromfile('/home/akshay/Desktop/576_Pro/CSCI576_Project_Database/'+args['dir']+'/'+file,dtype=np.uint8)
    ind = 0
    height = 288
    width = 352
    r=[]
    g=[]
    b=[]
    for y in range(height):
        for x in range(width):       
            r.append(a[ind])
            g.append(a[ind+height*width])
            b.append(a[ind+height*width*2])
            ind = ind+1
    big = []
    q = []
    for i in range(1,len(r)):
        if i%352==0:
            big.append(q)
            q=[]
        else:
            p = [b[i],g[i],r[i]]
            q.append(p)
            
    image = np.array(big)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    
    fgmask = fgbg.apply(image)
    fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)

    fgmask_f = fgmask.flatten().tolist()
    #print fgmask_f.count(127),fgmask_f.count(255)
   
    if chunk == chunk_size:
        chunk = 0
        print sum/chunk_size
        sum = 0
    chunk = chunk+1    
    sum = sum+np.count_nonzero(fgmask)

##    if chunk == chunk_size:
##        chunk = 0
##        print sum1/chunk_size,sum2/chunk_size
##        sum = 0
##    chunk = chunk+1    
##    sum1 = sum1+fgmask_f.count(127)
##    sum2 = sum2+fgmask_f.count(255)
    print fgmask
    cv2.imshow('frame',fgmask)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cv2.destroyAllWindows()
