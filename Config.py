__author__ = 'arjunpola'

# Flower      = 1
# Interview   = 2
# Movie       = 3
# MusicVideo  = 4
# Sports      = 5
# Starcraft   = 6
# Traffic     = 7

Root = "/Users/arjunpola/PycharmProjects/MediaSearch/Dataset/";

Paths = {  0 : Root+"test/",
           1 : Root+'flowers/',
           2 : Root+"interview/",
           3 : Root+"movie/",
           4 : Root+"musicvideo/",
           5 : Root+"sports/",
           6 : Root+"starcraft/",
           7 : Root+"traffic/",
           11 : Root+"Q1_1/",
           12 : Root+"Q1_2/",
           13 : Root+"Q1_3/",
           14 : Root+"Q1_4/",
           15 : Root+"Q1_5/",
           16 : Root+"Q1_6/",
           17 : Root+"Q1_7/"
}

AudioPaths = {
        0 : '440_sine.wav',
        1 : 'flowers.wav',
        2 : 'interview.wav',
        3 : 'movie.wav',
        4 : 'musicvideo.wav',
        5 : 'sports.wav',
        6 : 'StarCraft.wav',
        7 : 'traffic.wav',
        11 : 'first.wav',
        12 : 'second.wav',
        13 : 'Q3.wav',
        14 : 'Q4.wav',
        15 : 'Q5.wav',
        16 : 'Q6.wav',
        17 : 'Q7.wav'
}


def getPath(type):
    return Paths[type]

def getAudioPath(type):
    return Paths[type]+AudioPaths[type]

